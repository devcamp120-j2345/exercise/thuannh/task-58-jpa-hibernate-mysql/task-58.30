package com.devcamp.customerlist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerlist.model.CCustomer;

public interface CCustomerRepository extends JpaRepository<CCustomer, Long>{
    
}
